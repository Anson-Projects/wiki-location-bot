use teloxide::prelude::*;
use teloxide::types::{ButtonRequest, KeyboardButton, KeyboardMarkup, MessageKind};
use teloxide::types::{MediaKind, Message};

mod openstreetmap;
mod wikipedia;

fn help_text() -> String {
    r#"
This is a simple bot that takes a location and returns all of the nearby locations that have a wikipedia page\.

The bot can either be used by the `Send Location` button, sending a location from the share menu, or typing an address\.

This bot was made possible by:
    [OSM Foundation](https://nominatim.org/) for location searching
    [Wikipedia](https://www.wikipedia.org/) for having great APIs
"#
    .to_owned()
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    log::info!("Starting Wiki Locations Bot");

    let bot = Bot::from_env();

    teloxide::repl(bot, |bot: Bot, msg: Message| async move {
        log::info!("Message received.");
        bot.send_chat_action(msg.chat.id, teloxide::types::ChatAction::Typing)
            .await
            .unwrap();

        let location_button = KeyboardButton {
            text: "Send Location".to_string(),
            request: Some(ButtonRequest::Location),
        };

        let location_button_markup = KeyboardMarkup::new([[location_button]])
            .one_time_keyboard(true)
            .resize_keyboard(true);

        match msg.kind {
            MessageKind::Common(ref common) => match &common.media_kind {
                MediaKind::Location(media_location) => {
                    let user_location = media_location.location;
                    log::info!("Location received.");
                    wikipedia::send_wikipedia_pages(user_location.latitude, user_location.longitude, bot, msg).await;
                }
                MediaKind::Text(media_text) => {
                    let text = &media_text.text;
                    if text.contains("/help") || text.contains("/start") {
                        bot.send_message(msg.chat.id, help_text())
                            .parse_mode(teloxide::types::ParseMode::MarkdownV2)
                            .reply_markup(location_button_markup)
                            .await
                            .unwrap();
                    } else {
                        match openstreetmap::geocode_text(text) {
                            Ok(location) => {
                                bot.send_location(msg.chat.id, location.lat, location.lon).await.unwrap();
                                bot.send_message(msg.chat.id, format!("Location found: {}", location.name))
                                    .await
                                    .unwrap();
                                wikipedia::send_wikipedia_pages(location.lat, location.lon, bot, msg).await;
                            }
                            Err(error) => {
                                log::warn!("Geocoding failed: {}", error);
                                bot.send_message(
                                    msg.chat.id,
                                    format!("Location query returned no matches:\n\t{}\nTry /help", text),
                                )
                                .await
                                .unwrap();
                            }
                        }
                    }
                }
                _ => {}
            },
            _ => {
                log::warn!("Unsupported message type recieved");
                bot.send_message(
                    msg.chat.id,
                    "Send a location or address to see nearby places that have a wikipedia page!",
                )
                .reply_markup(location_button_markup)
                .await
                .unwrap();
            }
        }
        Ok(())
    })
    .await;
}
