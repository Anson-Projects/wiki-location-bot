use serde::{Deserialize, Serialize};
use url::Url;

// None of the fields are guaranteed, most have a default.
// I think it's just easier to only include the variables I care
//  about for now.
#[derive(Debug, Serialize, Deserialize)]
pub struct Place {
    // place_id: i64,
    // licence: String,
    // osm_type: String,
    // osm_id: i64,
    lat: String,
    lon: String,
    // category: String,
    // #[serde(rename = "type")]
    // place_type: String,
    // place_rank: i32,
    // importance: f64,
    // addresstype: String,
    name: String,
    // display_name: String,
    // boundingbox: Vec<String>,
}

pub struct Location {
    pub name: String,
    pub lat: f64,
    pub lon: f64,
}

pub fn geocode_text(search_query: &str) -> Result<Location, &str> {
    let base_url = "https://nominatim.openstreetmap.org/search";

    let url = Url::parse_with_params(base_url, &[("q", search_query), ("format", "jsonv2"), ("limit", "1")])
        .expect("Failed to construct OSM URL");

    let response = ureq::get(url.as_str())
        .set("User-Agent", "Wiki Location Telegram Bot")
        .call()
        .expect("Request failed")
        .into_string()
        .expect("Failed to read response from OSM");

    let places: Vec<Place> = serde_json::from_str(&response).expect("Failed to parse JSON");

    if let Some(place) = places.first() {
        let name = place.name.clone();
        let lat: f64 = place.lat.parse().expect("Failed to parse latitude");
        let lon: f64 = place.lon.parse().expect("Failed to parse longitude");
        Ok(Location { name, lat, lon })
    } else {
        Err("Latitude and Longitude could not be found")
    }
}
