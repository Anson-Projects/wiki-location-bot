use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use teloxide::prelude::*;
use teloxide::types::Message;

#[derive(Debug, Deserialize, Serialize)]
pub struct PageInfo {
    pageid: usize,
    ns: usize,
    title: String,
    contentmodel: String,
    pagelanguage: String,
    pagelanguagehtmlcode: String,
    pagelanguagedir: String,
    touched: String,
    lastrevid: usize,
    length: usize,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct GeoSearch {
    pageid: usize,
    ns: usize,
    title: String,
    lat: f64,
    lon: f64,
    dist: f32,
    primary: Option<bool>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Query {
    geosearch: Option<Vec<GeoSearch>>,
    pages: Option<HashMap<String, PageInfo>>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Root {
    batchcomplete: bool,
    query: Query,
}

fn escape_markdown_v2(text: String) -> String {
    // https://core.telegram.org/bots/api#markdownv2-style
    text.chars().fold(String::new(), |mut acc, c| {
        match c {
            '_' | '*' | '[' | ']' | '(' | ')' | '~' | '`' | '>' | '#' | '+' | '-' | '=' | '|' | '{' | '}' | '.' | '!' => {
                acc.push('\\');
            }
            _ => {}
        }
        acc.push(c);
        acc
    })
}

pub async fn send_wikipedia_pages(latitude: f64, longitude: f64, bot: Bot, msg: Message) {
    let nearby_locations = ureq::get(&format!(
        concat!(
            "https://en.wikipedia.org/w/api.php",
            "?action=query&format=json&list=geosearch&formatversion=2&gscoord={}|{}&gsradius=10000&gslimit=5"
        ),
        latitude, longitude
    ))
    .set("User-Agent", "Wiki Location Telegram Bot")
    .call()
    .unwrap()
    .into_json::<Root>()
    .unwrap()
    .query
    .geosearch
    .unwrap();

    for location in nearby_locations {
        bot.send_location(msg.chat.id, location.lat, location.lon).await.unwrap();

        let url = teloxide::utils::markdown::link(
            &format!("http://en.wikipedia.org/?curid={}", location.pageid),
            &escape_markdown_v2(location.title),
        );
        let bold_url = teloxide::utils::markdown::bold(&url);
        bot.send_message(msg.chat.id, bold_url)
            .parse_mode(teloxide::types::ParseMode::MarkdownV2)
            .await
            .unwrap();
    }
}
