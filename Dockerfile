FROM rust:1.70 as builder

WORKDIR /usr/src/app

COPY . .
RUN cargo build --release

FROM debian:bullseye-slim

COPY --from=builder /usr/src/app/target/release/wiki_location_bot .
RUN apt-get update && apt-get install -y ca-certificates && rm -rf /var/lib/apt/lists/*


CMD ["./wiki_location_bot"]